require("chromedriver");
const assert = require("assert");
const { Builder, By, Key, until } = require("selenium-webdriver");
let sign_up_button;
(async function example() {
    try {
        const driver = await new Builder().forBrowser("chrome").build();
        await driver.get(
            "https://dev-wip.freshgrade.com/app/#/login/userIdentifier?returnUrl=%2F"
        );
        let buttons = await driver.findElements(By.css("button"));

        let sign_up_button = buttons.filter(
            async button => (await button.text) == "SIGN UP"
        );

        // figure out how to select the particular button
        await buttons[1].click();

        const test_points = [
            {
                first: "abc",
                last: "def",
                email: "abc@gmail.com",
                password: "temp1234@"
            },
            {
                first: "abc",
                last: "def",
                email: "abc@gmail.com",
                password: "temp1234@"
            },
            {
                first: "abc",
                last: "def",
                email: "abc@gmail.com",
                password: "temp1234@"
            }
        ];

        const testOne = test_points[0];
        const form_inputs = await driver.findElements(
            By.className("MuiInputBase-input MuiInput-input")
        );
        console.log(form_inputs.length);

        await form_inputs[0].sendKeys("phur" + Key.TAB);
        await form_inputs[1].sendKeys("sherpa" + Key.TAB);
        await form_inputs[2].sendKeys("abcde@gmail.com" + Key.TAB);
        await form_inputs[3].sendKeys("testPass3wert" + Key.TAB);
        await form_inputs[4].sendKeys("testPass3wert" + Key.TAB);

        // check the first check box
        const check_boxone = await driver.findElement(
            By.xpath(
                "/html/body/div/div[1]/div[2]/section/div/div[1]/div/div[4]/div[3]/form/div[1]/div[7]/div[1]/label/span[1]/span[1]/input"
            )
        );
        await check_boxone.click();

        // select the second checkbox
        const check_boxtwo = await driver.findElement(
            By.xpath(
                "/html/body/div/div[1]/div[2]/section/div/div[1]/div/div[4]/div[3]/form/div[1]/div[7]/div[2]/label/span[1]/span[1]/input"
            )
        );
        await check_boxtwo.click();
        

        const sign_up = await driver.findElement(By.xpath("/html/body/div/div[1]/div[2]/section/div/div[1]/div/div[4]/div[3]/form/button"));
        sign_up.click();

    } finally {
        //await driver.quit();
        console.log(sign_up_button);
    }
})();
